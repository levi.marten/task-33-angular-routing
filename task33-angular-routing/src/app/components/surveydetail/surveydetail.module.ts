import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { SurveydetailComponent } from "./surveydetail.component";

const routes: Routes = [
    {
        path: '',
        component: SurveydetailComponent
    }
]

@NgModule({
    imports: [ RouterModule.forChild( routes )],
    exports: [RouterModule ]
})
export class SurveydetailModule {}