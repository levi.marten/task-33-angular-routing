import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Router } from '@angular/router';
import { SessionService } from 'src/app/services/session/session.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [ Validators.required, Validators.minLength(2), Validators.maxLength(64), Validators.email ]),
    password: new FormControl('', [ Validators.required ])
  });

  isLoading: boolean = false;

  constructor(private auth: AuthService, private session: SessionService, private router: Router) { }

  ngOnInit(): void {
  }

  get username() {
    return this.loginForm.get('username');
  };

  get password() {
    return this.loginForm.get('password');
  };

  async onLoginClicked() {
    try {
      this.isLoading = true;
      const result: any = await this.auth.login(this.loginForm.value);
      result.status < 400 ? this.session.save(result.data.token) : null;
      result.status < 400 ? this.router.navigateByUrl('/dashboard') : null;

    } catch (e) {
      alert('Error: ' + e.error.error);

    } finally {
      this.isLoading = false;
    };
  };
};
