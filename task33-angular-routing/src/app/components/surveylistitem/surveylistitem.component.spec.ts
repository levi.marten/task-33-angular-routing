import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveylistitemComponent } from './surveylistitem.component';

describe('SurveylistitemComponent', () => {
  let component: SurveylistitemComponent;
  let fixture: ComponentFixture<SurveylistitemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveylistitemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveylistitemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
