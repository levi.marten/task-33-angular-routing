import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { SessionService } from 'src/app/services/session/session.service';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  registerForm: FormGroup = new FormGroup({
    username: new FormControl('', [Validators.required, Validators.minLength(2), Validators.maxLength(64), Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(12)])
  });

  isLoading: boolean = false;

  constructor(private auth: AuthService, private session: SessionService, private router: Router) { }

  get username() {
    return this.registerForm.get('username');
  };

  get password() {
    return this.registerForm.get('password');
  };

  ngOnInit(): void {
  }

  async onSignUpClicked() {
    try {
      this.isLoading = true;
      console.log(this.registerForm.value)
      const result: any = await this.auth.register(this.registerForm.value);
      result.status < 400 ? this.session.save(result.data.token) : null;
      result.status < 400 ? this.router.navigateByUrl('/dashboard') : null;

    } catch (e) {
      alert('Error: ' + e.error.error);

    } finally {
      this.isLoading = false;
    };
  };
};
