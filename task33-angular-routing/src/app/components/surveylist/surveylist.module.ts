import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { SurveylistComponent } from "./surveylist.component";
import { SurveylistitemComponent } from "../surveylistitem/surveylistitem.component"

const routes: Routes = [
    {
        path: '',
        component: SurveylistComponent
    }
]

@NgModule({
    declarations: [ SurveylistitemComponent ],
    imports: [ RouterModule.forChild( routes )],
    exports: [RouterModule ]
})
export class SurveylistModule {}