import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  constructor() { }


  save(token: string) {
    localStorage.setItem('sessionToken', token);
  };

  get() : string {
    return localStorage.getItem('sessionToken') || '';
  }
}
